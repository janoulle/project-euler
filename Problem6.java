/**
 * @author  Jane Ullah
 * @purpose Problem 6 - Project Euler
 * @date    12/26/2011
 * @site 	http://janetalkscode.com	
 */

public class Problem6 {

	public static void main(String[] args) {
		int sumSquares = 0, squareSums = 0;
		int num = 100;
		//Use summation formula for quadratic series: (n*(n+1)*(2n+1))/6
		sumSquares = (num*(num+1)*(2*num+1))/6;
		//Use Euler's trick for summations: (n(n+1))/2
		squareSums = (int) Math.pow(((num*(num+1))/2),2);
		System.out.println(squareSums - sumSquares);		
	}
}
