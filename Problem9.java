/**
 * @author  Jane Ullah
 * @purpose Problem 9 - Project Euler
 * @date    12/29/2011
 * @site    http://janetalkscode.com	
 */
public class Problem9 {

	public static void main(String[] args) {
		boolean matchFound = false;
		for (int a = 3; a <= 500; a++)	{
			for (int b = 4; b <= 500; b++)	{
				for (int c = 5; c <= 500; c++)	{
					if (c*c == a*a + b*b)	{
						if ((a+b+c) == 1000)	{
							System.out.println("Correct Match found: " + a + " " + b + " " + c);
							matchFound = true;
							break;
						}
					}
				}
				if (matchFound)	{
					break;
				}
			}
			if (matchFound)	{
				break;
			}
		}
	}
}
