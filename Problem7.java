/**
 * @author  Jane Ullah
 * @purpose Problem 7 - Project Euler
 * @date    12/27/2011
 * @site    http://janetalkscode.com	
 */
public class Problem7 {

	public static void main(String[] args) {
		int primeNum = 0, index = 0;
		boolean is10000 = false;
		for (int i = 2; !is10000; i++)	{
			if (index == 10001)	{
				System.out.println(primeNum);
				is10000 = true;
				continue;
			}
			primeNum = Prime(i);
			if (primeNum > 0)	{
				index++;
			}
		}
	}
	
	//Method to check if a number is prime; Returns -1 if the number is not a prime number
	private static int Prime(int num)	{
		int j;
		for (j = 1; (j < num/2) && ( !((num % (j+1)) == 0 )) ; j++);
		return (j >= num/2) ? num:-1;
	}
}
