/**
 * @author  Jane Ullah
 * @purpose Problem 3 - Project Euler
 * @date    12/19/2011
 * @site 	http://janetalkscode.com	
 */
public class Problem3 {

	public static void main(String[] args) {

		long primeNumber = 600851475143L, divisor = 0L, primeDivisor = 0L;
		long controller = (long) Math.floor(Math.sqrt(primeNumber));
		
		for (long i = 2L; i <= controller; i++ )	{
			//Catching all divisors of the number
			if (primeNumber % i == 0)	{
				divisor = i;
				//Checking for prime divisors.
				if (divisorTest(divisor))	{
					primeDivisor = divisor;
				}
			}
		}
		System.out.println("Largest Prime Divisor: " + primeDivisor);
	}
	
	//Method to test the 'prime-ness' of the divisors
	private static boolean divisorTest(long number){
		long j = 0;
		if (number == 2) { return true; }
		for (j = 0; (j <= number/2) && ( !((number % (j+2)) == 0 )) ; j++);
		return (j > number/2);
	}
}
