/**
 * @author  Jane Ullah
 * @purpose Problem 5 - Project Euler
 * @date    12/26/2011
 * @site 	http://janetalkscode.com	
 */
public class Problem5 {

	public static void main(String[] args) {
		int sum = 1, primeNum = 0;
		for (int i = 1; i <= 20; i++)	{
			primeNum = Prime(i);
			if (primeNum > 0)	{
				sum *= primeNum;
			}
			while ( primeNum < 0 && !(sum%i == 0) )	{
				//Multiply with 2 or 3. Between 18 and 20, the divisors to worry
				//about are: 2, 3, 5 (5 is a prime).
				if ((sum*2)%i == 0)		{
					sum *= 2;
				}
				else if ((sum*3)%i == 0)	{
					sum *= 3;
				}
			}
		}
		System.out.println("Smallest Divisible Number: " + sum);
	}
	
	//Method to find out if the argument is a prime number; 
	//Returns the argument if the number is a prime and returns -1 if the argument is not a prime.
	private static int Prime(int num)	{
		int j;
		if (num ==  2 || num == 3)	{	return num; }
		for (j = 0; (j <= num/2) && ( !((num % (j+2)) == 0 )) ; j++);
		return (j > num/2) ? num:-1;
	}

}
